//
//  NavViewController.swift
//  DemoTask
//
//  Created by MacbookPro on 04/10/19.
//  Copyright © 2019 MacbookPro. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionCell: UICollectionViewCell {
    @IBOutlet weak var imagView: UIImageView!
}

class NavViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}

extension NavViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionCell
        
        let Url = NSURL(string: "https://akm-img-a-in.tosshub.com/indiatoday/images/story/201909/Mobile-Reuters_0-770x433.jpeg?iIpLeITw9L_ae.O.v_UVe.CrwDhNMZ.k")
        cell.imagView.sd_setImage(with: Url! as URL,placeholderImage: nil,options: [SDWebImageOptions.progressiveDownload, SDWebImageOptions.refreshCached],completed: { (image, error, cacheType, url) in
        })
        
        return cell
    }
}
