//
//  CountryViewController.swift
//  DemoTask
//
//  Created by MacbookPro on 04/10/19.
//  Copyright © 2019 MacbookPro. All rights reserved.
//

import UIKit
import MBProgressHUD

class CountryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var appDel: AppDelegate! = (UIApplication.shared.delegate as! AppDelegate)
    
    var countryList = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.appDel.apiManager.setCurrentViewController(vc: self)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.get_County_List_API(url: "json", request_Type: "GET")
    }
    
    //------------------------------------------------------
    // MARK:- API Calling
    //------------------------------------------------------
    
    private func get_County_List_API(url: String, request_Type: String) {
    
        appDel.apiManager.api_Calling_Get_Request(url: url, request_Type: request_Type, onComplete: {
            (details, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if details != nil{
                let detailDict = details as! NSDictionary
                self.countryList = detailDict.value(forKey: "countries") as! NSArray
                self.tableView.reloadData()
            }else{
                print("Server is not responding, please try again later.")
            }
        })
    }
}

//------------------------------------------------------
// MARK:- UITableview
//------------------------------------------------------
extension CountryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.textLabel?.text = (((self.countryList.object(at: indexPath.row) as AnyObject).value(forKey: "country") as? NSDictionary)?.value(forKey: "country_name") as? String ?? "")
        return cell
    }
}
