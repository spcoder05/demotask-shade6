//
//  ViewController.swift
//  DemoTask
//
//  Created by MacbookPro on 04/10/19.
//  Copyright © 2019 MacbookPro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arr = ["Navigation Controller", "Country List"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupUI()
    }
    
    func setupUI() {
        
    }
}

extension ViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.arr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
        }else{
            
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
}

