//
//  APIServiceManager.swift
//  DemoTask
//
//  Created by MacbookPro on 04/10/19.
//  Copyright © 2019 MacbookPro. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

typealias ServiceResponse = (Any?, NSError?) -> Void

class APIServiceManager: NSObject {
    
    var root_url: String!
    var session: SessionManager!
    var currentViewController: UIViewController?
    
    init(root_url: String!, vc: UIViewController?){
        self.root_url = root_url
        self.currentViewController = vc
    }
    
    func setCurrentViewController(vc: UIViewController?){
        self.currentViewController = vc
    }
    
    //************************************************************
    
    //MARK: GET Request Type API
    
    //************************************************************
    
    func api_Calling_Get_Request(url: String, request_Type: String, onComplete: ServiceResponse?){
        
        let URL = root_url + url
        
        let type = HTTPMethod(rawValue: request_Type)
        
        var headers = Alamofire.SessionManager.defaultHTTPHeaders
        headers["Content-Type"] = ("application/json")
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = headers
        
        self.session = Alamofire.SessionManager(configuration: configuration)
        
        self.session.request(URL,method: type!,parameters: nil
            ).responseJSON(completionHandler: {(response) in
                debugPrint(" response is:>",response)
                if response.result.isSuccess && response.response?.statusCode == 200{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 401{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 400{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 403{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 404{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 429{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else if response.result.isSuccess && response.response?.statusCode == 500{
                    let details  = response.result.value!
                    onComplete!(details,nil )
                }else{
                    onComplete!(nil,nil )
                }
            })
    }
}
